﻿using ClientsApp.Core.Models;
using System.Collections.Generic;

namespace ClientsApp.Core.Interfaces.Repositories
{
    public interface IRepository
    {
        IEnumerable<User> All();

        void Add(User entity);

        void Delete(int id);

        User GetById(int id);
    }
}
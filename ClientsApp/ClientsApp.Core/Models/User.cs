﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientsApp.Core.Models
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public List<string> PhoneNumber { get; set; }

        public List<Task> Tasks { get; set; }

    }
}

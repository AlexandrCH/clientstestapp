﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientsApp.Core.Models
{
    public class Task : BaseEntity
    {
        public string TaskName { get; set; }

        public string Description { get; set; }

        public string ClientAddress { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

    }
}

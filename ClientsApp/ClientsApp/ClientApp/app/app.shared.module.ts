import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { TaskComponent } from './components/task/task.component';
import { UserService } from './components/shared/user.service';
import { FilterPipe } from './components/shared/filter.pipe';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        UserComponent,
        TaskComponent,
        FilterPipe
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'user', component: UserComponent },
            { path: "task/:id", component: TaskComponent },
            { path: 'task', component: TaskComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    exports: [RouterModule],
    providers: [UserService]
})
export class AppModuleShared {
}

﻿import { Injectable, Inject } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { User } from "../user/user.class";
import { environment } from "../../../enviroment";

@Injectable()
export class UserService {
    baseUrl: string 

    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    getUser(id: number): Observable<User> {

        const Url = environment.apiGetUser + id;

        console.log(Url);
        return this.http.get(Url).map(response => {
            const user: User = new User();
            const data = response.json();
            user.address = data.address;
            user.firstName = data.firstName;
            user.lastName = data.lastName;
            user.tasks = data.tasks;
            user.phoneNumber = data.phoneNumber;
            user.id = data.id
            return user;
        });
    }

    getAllUsers() : Observable<User[]>{
        return this.http.get(this.baseUrl + 'api/User/GetUsers').map(response => {
            let users: User[];
            const data = response.json();
            users = data;
            return users;
        });
    }

    delete(id: number){
        const Url = "http://localhost:17123/" + "api/User/Delete/" + id;
        return this.http.get(Url).subscribe(
            (response) => {
                console.log("VALUE RECEIVED: " + response);
            },
            (err) => {
                console.log("ERROR: " + err);
            });
    }
}

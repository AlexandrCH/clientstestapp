import { Component, Inject, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { User } from '../user/user.class';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
import { UserService } from '../shared/user.service';

@Component({
    selector: 'task',
    templateUrl: './task.component.html'
})

export class TaskComponent implements OnInit {

    public checkedUser: User = new User();

    constructor(private service: UserService, private currentRoute: ActivatedRoute) {

    }
    ngOnInit() {

        this.initialize();
    }
    onDelete(id: number) {
        this.service.delete(id);
        this.initialize();
    }

    initialize() {
        this.currentRoute.params.forEach((params: Params) => {
            let id = +params['id'];
            this.service.getUser(id).subscribe(x => { this.checkedUser = x }, () => console.log("Error"));
        })
    }
}

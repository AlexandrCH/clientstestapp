export interface Task
{
    taskName: string;
    description: string;
    clientAddress: string;
    startTime: string;
    endTime: string;
    id: number;
}
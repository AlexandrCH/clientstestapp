import { Component, Inject, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user.class';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { componentFactoryName } from '@angular/compiler';

@Component({
    selector: 'user',
    templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
    
    //public values: string[];
    //public value: string;
    //public user: number;
    //public city: number;
    public users: User[];
    boolHelper: boolean = false;
    public selectedUser: User = new User();

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, private router: Router, private service: UserService) {
    }

    ngOnInit(): void {
        this.service.getAllUsers().subscribe(x => { this.users = x }, () => { console.log("Error") });
    }

    onTaskInfo(id: number) {
        this.router.navigate(["task", id]);
    }

    onSelectUser(id: number) {
       //this.city = 0;
        console.log(id);
        this.service.getUser(id).subscribe(x => { this.selectedUser = x }, () => { console.log("Error"), this.boolHelper = false; });
       
        this.boolHelper = true;
        console.log(this.selectedUser);
    }
}
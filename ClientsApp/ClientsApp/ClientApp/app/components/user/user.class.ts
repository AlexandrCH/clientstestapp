import { Task } from "../task/task.interface";

export class User {
    address: string;
    firstName: string;
    lastName: string;
    tasks: Task;
    phoneNumber: string[];
    id: number;
}

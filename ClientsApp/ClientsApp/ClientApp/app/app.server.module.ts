import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { RouterModule } from '@angular/router';
import { UserService } from './components/shared/user.service';

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        ServerModule,
        AppModuleShared,
        RouterModule
    ],
    providers: [UserService]
})
export class AppModule {
}

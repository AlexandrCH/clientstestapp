﻿using System.Collections.Generic;
using ClientsApp.Core.Interfaces.Repositories;
using ClientsApp.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClientsApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IRepository repository;
        public UserController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet("[action]")]
        public IEnumerable<User> GetUsers()
        {
            return repository.All();
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetUser(int id)
        {

            if (repository.GetById(id) != null)
            {
                return Ok(repository.GetById(id));
            }
            return NotFound();
        }

        [HttpGet("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            repository.Delete(id);
            return Ok();
        }
    }
}
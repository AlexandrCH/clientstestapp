﻿using ClientsApp.Core.Interfaces.Repositories;
using ClientsApp.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClientsApp.Dependencies
{
    public static class DependenciesConfigurator
    {
        public static void AddDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IRepository, Repository>();
        }
    }
}
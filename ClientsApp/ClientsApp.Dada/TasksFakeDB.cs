﻿using ClientsApp.Core.Models;
using System;
using System.Collections.Generic;

namespace ClientsApp.Dada
{
    public class TasksFakeDB
    {
        public static List<Task> userTasks1 = new List<Task>(){
            new Task() {
                Id = 1,
                ClientAddress ="New York 110 West 13th Street",
                Description ="Some description 1",
                TaskName = "Random task 1",
                EndTime = "7:25:18",
                StartTime = "8:45:12"
            },

            new Task() {
                Id = 2,
                ClientAddress ="New York 115 West 13th Street",
                Description ="Some description 2",
                TaskName = "Random task 2",
                EndTime = "11:35:45",
                StartTime = "8:45:12"
            },

            new Task() {
                Id = 3,
                ClientAddress ="London 123 West 11th Street",
                Description ="Some description 3",
                TaskName = "Random task 3",
                EndTime = "11:26:45",
                StartTime = "7:35:12"
            },
        };

        public static List<Task> userTasks2 = new List<Task>(){
            new Task() {
                Id = 4,
                ClientAddress ="London 101 West 14th Street",
                Description ="Some description 4",
                TaskName = "Random task 4",
                EndTime = "13:26:45",
                StartTime = "17:35:12"
            },
            new Task() {
                Id = 5,
                ClientAddress ="New York 102 West 16th Street",
                Description ="Some description 5",
                TaskName = "Random task 5",
                EndTime = "14:26:45",
                StartTime = "16:35:14"
            },
        };

        public static List<Task> userTasks3 = new List<Task>(){
            new Task() {
                Id = 6,
                ClientAddress ="13 West 14th Street",
                Description ="Some description 6",
                TaskName = "Random task 6",
                EndTime = "13:26:45",
                StartTime = "12:31:12"
            },
            new Task() {
                Id = 7,
                ClientAddress ="102 West 16th Street",
                Description ="Some description 7",
                TaskName = "Random task 7",
                EndTime = "14:36:45",
                StartTime = "11:35:14"
            },
            new Task() {
                Id = 8,
                ClientAddress ="99 West 14th Street",
                Description ="Some description 8",
                TaskName = "Random task 8",
                EndTime = "13:26:45",
                StartTime = "17:35:12"
            },
            new Task() {
                Id = 9,
                ClientAddress ="20 West 16th Street",
                Description ="Some description 9",
                TaskName = "Random task 9",
                EndTime = "22:26:45",
                StartTime = "09:35:18"
            },
        };

        public static List<Task> userTasks4 = new List<Task>(){
            new Task() {
                Id = 11,
                ClientAddress ="45 West 10th Street",
                Description ="Some description 11",
                TaskName = "Random task 11",
                EndTime = "23:26:45",
                StartTime = "11:35:11"
            },

            new Task() {
                Id = 12,
                ClientAddress ="20 West 16th Street",
                Description ="Some description 12",
                TaskName = "Random task 12",
                EndTime = "07:26:45",
                StartTime = "07:35:14"
            },
        };

        public static List<Task> userTasks5 = new List<Task>(){
            new Task() {
                Id = 13,
                ClientAddress ="145 West 11th Street",
                Description ="Some description 13",
                TaskName = "Random task 13",
                EndTime = "16:21:41",
                StartTime = "12:31:11"
            },
        };
    }
}

﻿using ClientsApp.Core.Models;
using System;
using System.Collections.Generic;

namespace ClientsApp.Dada
{
    public class PhoneNumbersFakeDB
    {
        public static List<string> phoneNumbers1 = new List<string>
        {
            "59 565 456 898",
            "38 123 456 567",
            "06 567 123 123",
            "07 123 345 346",
        };

        public static List<string> phoneNumbers2 = new List<string>
        {
            "7 562 875 566",
            "8 744 888 444",
        };

        public static List<string> phoneNumbers3 = new List<string>
        {
            "7 888 777 666",
            "38 123 456 456",
            "06 575 123 756",
            "06 123 345 888",
            "01 784 564 333",
        };

        public static List<string> phoneNumbers4 = new List<string>
        {
            "44 677 222 444",
        };

        public static List<string> phoneNumbers5 = new List<string>
        {
            "08 477 889 000",
            "02 123 655 223",
            "09 223 123 333",
            "05 444 345 655",
        };

    }
}

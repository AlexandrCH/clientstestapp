﻿using ClientsApp.Core.Models;
using System;
using System.Collections.Generic;

namespace ClientsApp.Dada
{
    public class UsersFakeDB
    {
        public static List<User> Users = new List<User> {
              new User() {
                  Id = 1,
                  Address ="New York 25 West 11th Street",
                  FirstName ="Alex",
                  LastName = "Timbarlake",
                  PhoneNumber = PhoneNumbersFakeDB.phoneNumbers1,
                  Tasks = TasksFakeDB.userTasks1
              },
              new User() {
                  Id = 2,
                  Address ="London 5 West 13th Street",
                  FirstName ="Mark",
                  LastName = "Spears",
                  PhoneNumber = PhoneNumbersFakeDB.phoneNumbers2,
                  Tasks = TasksFakeDB.userTasks2
              },
              new User() {
                  Id = 3,
                  Address ="London 103 West 11th Street",
                  FirstName ="Rhonda",
                  LastName = "Burchfield",
                  PhoneNumber = PhoneNumbersFakeDB.phoneNumbers3,
                  Tasks = TasksFakeDB.userTasks3
              },
              new User() {
                  Id = 4,
                  Address ="New York 15 West 11th Street",
                  FirstName ="David",
                  LastName = "Show",
                  PhoneNumber = PhoneNumbersFakeDB.phoneNumbers4,
                  Tasks = TasksFakeDB.userTasks4
              },
              new User() {
                  Id = 5,
                  Address ="New York 193 West 12th Street",
                  FirstName ="Trevor",
                  LastName = "Blain",
                  PhoneNumber = PhoneNumbersFakeDB.phoneNumbers5,
                  Tasks = TasksFakeDB.userTasks5
              },
        };
    }
}

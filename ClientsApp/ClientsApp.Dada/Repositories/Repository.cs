﻿using ClientsApp.Core.Interfaces.Repositories;
using ClientsApp.Core.Models;
using ClientsApp.Dada;
using System.Collections.Generic;
using System.Linq;

namespace ClientsApp.Data.Repositories
{
    public class Repository : IRepository
    {
        public void Add(User entity)
        {
            UsersFakeDB.Users.Add(entity);
        }

        public IEnumerable<User> All()
        {
            return UsersFakeDB.Users;
        }

        public void Delete(int id)
        {
            foreach (var user in UsersFakeDB.Users)
            {
                for (int i = 0; i < user.Tasks.Count; i++)
                {
                    if (user.Tasks[i].Id == id)
                    {
                        user.Tasks.Remove(user.Tasks[i]);
                    }
                }
            }
        }

        public User GetById(int id)
        {
            foreach (var user in UsersFakeDB.Users)
            {
                if (user.Id == id)
                {
                    return user;
                }
            }
            return null;
        }
    }
}